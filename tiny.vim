" Project: Tiny Vim
" License: MIT

if version > 580
  hi clear
  if exists("syntax_on")
    syntax reset
  endif
endif

let g:colors_name = "tiny"
let s:tiny_vim_version="0.9.0"
set background=dark

let s:tiny0_gui = "#343232"
let s:tiny1_gui = "#f06f78"
" let s:tiny2_gui = "#82dc78"
let s:tiny2_gui = "#900000"
let s:tiny3_gui = "#f0a641"
let s:tiny4_gui = "#82a6af"
let s:tiny5_gui = "#b96faf"
let s:tiny6_gui = "#b9dc41"
let s:tiny7_gui = "#a89984"
let s:tiny8_gui = "#928374"
let s:tiny9_gui = "#fa9a92"
let s:tiny10_gui = "#ade692"
let s:tiny11_gui = "#fac06c"
let s:tiny12_gui = "#adc0b9"
let s:tiny13_gui = "#d39ab9"
let s:tiny14_gui = "#d3e66c"
let s:tiny15_gui = "#fff2c6"

let s:tiny1_term = "0"
let s:tiny3_term = "8"
let s:tiny5_term = "7"
let s:tiny6_term = "15"
let s:tiny7_term = "14"
let s:tiny8_term = "6"
let s:tiny9_term = "4"
let s:tiny10_term = "12"
let s:tiny11_term = "1"
let s:tiny12_term = "11"
let s:tiny13_term = "3"
let s:tiny14_term = "2"
let s:tiny15_term = "5"

let s:tiny3_gui_brightened = [
  \ s:tiny3_gui,
  \ "#4e586d",
  \ "#505b70",
  \ "#525d73",
  \ "#556076",
  \ "#576279",
  \ "#59647c",
  \ "#5b677f",
  \ "#5d6982",
  \ "#5f6c85",
  \ "#616e88",
  \ "#63718b",
  \ "#66738e",
  \ "#687591",
  \ "#6a7894",
  \ "#6d7a96",
  \ "#6f7d98",
  \ "#72809a",
  \ "#75829c",
  \ "#78859e",
  \ "#7b88a1",
\ ]

if !exists("g:tiny_italic")
  if has("gui_running") || $TERM_ITALICS == "true"
    let g:tiny_italic=1
  else
    let g:tiny_italic=0
  endif
endif

let s:italic = "italic,"
if g:tiny_italic == 0
  let s:italic = ""
endif

let s:underline = "underline,"
if ! get(g:, "tiny_underline", 1)
  let s:underline = "NONE,"
endif

let s:italicize_comments = ""
if exists("g:tiny_italic_comments")
  if g:tiny_italic_comments == 1
    let s:italicize_comments = s:italic
  endif
endif

if !exists('g:tiny_uniform_status_lines')
  let g:tiny_uniform_status_lines = 0
endif

if !exists("g:tiny_comment_brightness")
  let g:tiny_comment_brightness = 0
endif

if !exists("g:tiny_uniform_diff_background")
  let g:tiny_uniform_diff_background = 0
endif

if !exists("g:tiny_cursor_line_number_background")
  let g:tiny_cursor_line_number_background = 0
endif

function! s:hi(group, guifg, guibg, ctermfg, ctermbg, attr, guisp)
  if a:guifg != ""
    exec "hi " . a:group . " guifg=" . a:guifg
  endif
  if a:guibg != ""
    exec "hi " . a:group . " guibg=" . a:guibg
  endif
  if a:ctermfg != ""
    exec "hi " . a:group . " ctermfg=" . a:ctermfg
  endif
  if a:ctermbg != ""
    exec "hi " . a:group . " ctermbg=" . a:ctermbg
  endif
  if a:attr != ""
    exec "hi " . a:group . " gui=" . a:attr . " cterm=" . substitute(a:attr, "undercurl", s:underline, "")
  endif
  if a:guisp != ""
    exec "hi " . a:group . " guisp=" . a:guisp
  endif
endfunction

"+---------------+
"+ UI Components +
"+---------------+
"+--- Attributes ---+
call s:hi("Bold", "", "", "", "", "bold", "")
call s:hi("Italic", "", "", "", "", s:italic, "")
call s:hi("Underline", "", "", "", "", s:underline, "")

"+--- Editor ---+
call s:hi("ColorColumn", "", s:tiny1_gui, "NONE", s:tiny1_term, "", "")
call s:hi("Cursor", s:tiny0_gui, s:tiny4_gui, "", "NONE", "", "")
call s:hi("CursorLine", "", s:tiny1_gui, "NONE", s:tiny1_term, "NONE", "")
call s:hi("Error", s:tiny0_gui, s:tiny11_gui, "", s:tiny11_term, "", "")
call s:hi("iCursor", s:tiny0_gui, s:tiny4_gui, "", "NONE", "", "")
call s:hi("LineNr", s:tiny3_gui, s:tiny0_gui, s:tiny3_term, "NONE", "", "")
call s:hi("MatchParen", s:tiny8_gui, s:tiny3_gui, s:tiny8_term, s:tiny3_term, "", "")
call s:hi("NonText", s:tiny2_gui, "", s:tiny3_term, "", "", "")
call s:hi("Normal", s:tiny4_gui, s:tiny0_gui, "NONE", "NONE", "", "")
call s:hi("PMenu", s:tiny4_gui, s:tiny2_gui, "NONE", s:tiny1_term, "NONE", "")
call s:hi("PmenuSbar", s:tiny4_gui, s:tiny2_gui, "NONE", s:tiny1_term, "", "")
call s:hi("PMenuSel", s:tiny8_gui, s:tiny3_gui, s:tiny8_term, s:tiny3_term, "", "")
call s:hi("PmenuThumb", s:tiny8_gui, s:tiny3_gui, "NONE", s:tiny3_term, "", "")
call s:hi("SpecialKey", s:tiny3_gui, "", s:tiny3_term, "", "", "")
call s:hi("SpellBad", s:tiny11_gui, s:tiny0_gui, s:tiny11_term, "NONE", "undercurl", s:tiny11_gui)
call s:hi("SpellCap", s:tiny13_gui, s:tiny0_gui, s:tiny13_term, "NONE", "undercurl", s:tiny13_gui)
call s:hi("SpellLocal", s:tiny5_gui, s:tiny0_gui, s:tiny5_term, "NONE", "undercurl", s:tiny5_gui)
call s:hi("SpellRare", s:tiny6_gui, s:tiny0_gui, s:tiny6_term, "NONE", "undercurl", s:tiny6_gui)
call s:hi("Visual", "", s:tiny2_gui, "", s:tiny1_term, "", "")
call s:hi("VisualNOS", "", s:tiny2_gui, "", s:tiny1_term, "", "")
"+- Neovim Support -+
call s:hi("healthError", s:tiny11_gui, s:tiny1_gui, s:tiny11_term, s:tiny1_term, "", "")
call s:hi("healthSuccess", s:tiny14_gui, s:tiny1_gui, s:tiny14_term, s:tiny1_term, "", "")
call s:hi("healthWarning", s:tiny13_gui, s:tiny1_gui, s:tiny13_term, s:tiny1_term, "", "")
call s:hi("TermCursorNC", "", s:tiny1_gui, "", s:tiny1_term, "", "")

"+- Neovim Terminal Colors -+
if has('nvim')
  let g:terminal_color_0 = s:tiny1_gui
  let g:terminal_color_1 = s:tiny11_gui
  let g:terminal_color_2 = s:tiny14_gui
  let g:terminal_color_3 = s:tiny13_gui
  let g:terminal_color_4 = s:tiny9_gui
  let g:terminal_color_5 = s:tiny15_gui
  let g:terminal_color_6 = s:tiny8_gui
  let g:terminal_color_7 = s:tiny5_gui
  let g:terminal_color_8 = s:tiny3_gui
  let g:terminal_color_9 = s:tiny11_gui
  let g:terminal_color_10 = s:tiny14_gui
  let g:terminal_color_11 = s:tiny13_gui
  let g:terminal_color_12 = s:tiny9_gui
  let g:terminal_color_13 = s:tiny15_gui
  let g:terminal_color_14 = s:tiny7_gui
  let g:terminal_color_15 = s:tiny6_gui
endif

"+--- Gutter ---+
call s:hi("CursorColumn", "", s:tiny1_gui, "NONE", s:tiny1_term, "", "")
if g:tiny_cursor_line_number_background == 0
  call s:hi("CursorLineNr", s:tiny4_gui, s:tiny0_gui, "NONE", "", "", "")
else
  call s:hi("CursorLineNr", s:tiny4_gui, s:tiny1_gui, "NONE", s:tiny1_term, "", "")
endif
call s:hi("Folded", s:tiny3_gui, s:tiny1_gui, s:tiny3_term, s:tiny1_term, "bold", "")
call s:hi("FoldColumn", s:tiny3_gui, s:tiny0_gui, s:tiny3_term, "NONE", "", "")
call s:hi("SignColumn", s:tiny1_gui, s:tiny0_gui, s:tiny1_term, "NONE", "", "")

"+--- Navigation ---+
call s:hi("Directory", s:tiny8_gui, "", s:tiny8_term, "NONE", "", "")

"+--- Prompt/Status ---+
call s:hi("EndOfBuffer", s:tiny1_gui, "", s:tiny1_term, "NONE", "", "")
call s:hi("ErrorMsg", s:tiny4_gui, s:tiny11_gui, "NONE", s:tiny11_term, "", "")
call s:hi("ModeMsg", s:tiny4_gui, "", "", "", "", "")
call s:hi("MoreMsg", s:tiny4_gui, "", "", "", "", "")
call s:hi("Question", s:tiny4_gui, "", "NONE", "", "", "")
if g:tiny_uniform_status_lines == 0
  call s:hi("StatusLine", s:tiny8_gui, s:tiny3_gui, s:tiny8_term, s:tiny3_term, "NONE", "")
  call s:hi("StatusLineNC", s:tiny4_gui, s:tiny1_gui, "NONE", s:tiny1_term, "NONE", "")
  call s:hi("StatusLineTerm", s:tiny8_gui, s:tiny3_gui, s:tiny8_term, s:tiny3_term, "NONE", "")
  call s:hi("StatusLineTermNC", s:tiny4_gui, s:tiny1_gui, "NONE", s:tiny1_term, "NONE", "")
else
  call s:hi("StatusLine", s:tiny8_gui, s:tiny3_gui, s:tiny8_term, s:tiny3_term, "NONE", "")
  call s:hi("StatusLineNC", s:tiny4_gui, s:tiny3_gui, "NONE", s:tiny3_term, "NONE", "")
  call s:hi("StatusLineTerm", s:tiny8_gui, s:tiny3_gui, s:tiny8_term, s:tiny3_term, "NONE", "")
  call s:hi("StatusLineTermNC", s:tiny4_gui, s:tiny3_gui, "NONE", s:tiny3_term, "NONE", "")
endif
call s:hi("WarningMsg", s:tiny0_gui, s:tiny13_gui, s:tiny1_term, s:tiny13_term, "", "")
call s:hi("WildMenu", s:tiny8_gui, s:tiny1_gui, s:tiny8_term, s:tiny1_term, "", "")

"+--- Search ---+
call s:hi("IncSearch", s:tiny1_gui, s:tiny8_gui, s:tiny1_term, s:tiny8_term, s:underline, "")
call s:hi("Search", s:tiny1_gui, s:tiny8_gui, s:tiny1_term, s:tiny8_term, "NONE", "")

"+--- Tabs ---+
call s:hi("TabLine", s:tiny4_gui, s:tiny1_gui, "NONE", s:tiny1_term, "NONE", "")
call s:hi("TabLineFill", s:tiny4_gui, s:tiny1_gui, "NONE", s:tiny1_term, "NONE", "")
call s:hi("TabLineSel", s:tiny8_gui, s:tiny3_gui, s:tiny8_term, s:tiny3_term, "NONE", "")

"+--- Window ---+
call s:hi("Title", s:tiny4_gui, "", "NONE", "", "NONE", "")
call s:hi("VertSplit", s:tiny2_gui, s:tiny1_gui, s:tiny3_term, s:tiny1_term, "NONE", "")

"+----------------------+
"+ Language Base Groups +
"+----------------------+
call s:hi("Boolean", s:tiny9_gui, "", s:tiny9_term, "", "", "")
call s:hi("Character", s:tiny14_gui, "", s:tiny14_term, "", "", "")
call s:hi("Comment", s:tiny3_gui_brightened[g:tiny_comment_brightness], "", s:tiny3_term, "", s:italicize_comments, "")
call s:hi("Conditional", s:tiny9_gui, "", s:tiny9_term, "", "", "")
call s:hi("Constant", s:tiny4_gui, "", "NONE", "", "", "")
call s:hi("Define", s:tiny9_gui, "", s:tiny9_term, "", "", "")
call s:hi("Delimiter", s:tiny6_gui, "", s:tiny6_term, "", "", "")
call s:hi("Exception", s:tiny9_gui, "", s:tiny9_term, "", "", "")
call s:hi("Float", s:tiny15_gui, "", s:tiny15_term, "", "", "")
call s:hi("Function", s:tiny8_gui, "", s:tiny8_term, "", "", "")
call s:hi("Identifier", s:tiny4_gui, "", "NONE", "", "NONE", "")
call s:hi("Include", s:tiny9_gui, "", s:tiny9_term, "", "", "")
call s:hi("Keyword", s:tiny9_gui, "", s:tiny9_term, "", "", "")
call s:hi("Label", s:tiny9_gui, "", s:tiny9_term, "", "", "")
call s:hi("Number", s:tiny15_gui, "", s:tiny15_term, "", "", "")
call s:hi("Operator", s:tiny9_gui, "", s:tiny9_term, "", "NONE", "")
call s:hi("PreProc", s:tiny9_gui, "", s:tiny9_term, "", "NONE", "")
call s:hi("Repeat", s:tiny9_gui, "", s:tiny9_term, "", "", "")
call s:hi("Special", s:tiny4_gui, "", "NONE", "", "", "")
call s:hi("SpecialChar", s:tiny13_gui, "", s:tiny13_term, "", "", "")
call s:hi("SpecialComment", s:tiny8_gui, "", s:tiny8_term, "", s:italicize_comments, "")
call s:hi("Statement", s:tiny9_gui, "", s:tiny9_term, "", "", "")
call s:hi("StorageClass", s:tiny9_gui, "", s:tiny9_term, "", "", "")
call s:hi("String", s:tiny14_gui, "", s:tiny14_term, "", "", "")
call s:hi("Structure", s:tiny9_gui, "", s:tiny9_term, "", "", "")
call s:hi("Tag", s:tiny4_gui, "", "", "", "", "")
call s:hi("Todo", s:tiny13_gui, "NONE", s:tiny13_term, "NONE", "", "")
call s:hi("Type", s:tiny9_gui, "", s:tiny9_term, "", "NONE", "")
call s:hi("Typedef", s:tiny9_gui, "", s:tiny9_term, "", "", "")
hi! link Macro Define
hi! link PreCondit PreProc

"+-----------+
"+ Languages +
"+-----------+
call s:hi("awkCharClass", s:tiny7_gui, "", s:tiny7_term, "", "", "")
call s:hi("awkPatterns", s:tiny9_gui, "", s:tiny9_term, "", "bold", "")
hi! link awkArrayElement Identifier
hi! link awkBoolLogic Keyword
hi! link awkBrktRegExp SpecialChar
hi! link awkComma Delimiter
hi! link awkExpression Keyword
hi! link awkFieldVars Identifier
hi! link awkLineSkip Keyword
hi! link awkOperator Operator
hi! link awkRegExp SpecialChar
hi! link awkSearch Keyword
hi! link awkSemicolon Delimiter
hi! link awkSpecialCharacter SpecialChar
hi! link awkSpecialPrintf SpecialChar
hi! link awkVariables Identifier

call s:hi("cIncluded", s:tiny7_gui, "", s:tiny7_term, "", "", "")
hi! link cOperator Operator
hi! link cPreCondit PreCondit

hi! link csPreCondit PreCondit
hi! link csType Type
hi! link csXmlTag SpecialComment

call s:hi("cssAttributeSelector", s:tiny7_gui, "", s:tiny7_term, "", "", "")
call s:hi("cssDefinition", s:tiny7_gui, "", s:tiny7_term, "", "NONE", "")
call s:hi("cssIdentifier", s:tiny7_gui, "", s:tiny7_term, "", s:underline, "")
call s:hi("cssStringQ", s:tiny7_gui, "", s:tiny7_term, "", "", "")
hi! link cssAttr Keyword
hi! link cssBraces Delimiter
hi! link cssClassName cssDefinition
hi! link cssColor Number
hi! link cssProp cssDefinition
hi! link cssPseudoClass cssDefinition
hi! link cssPseudoClassId cssPseudoClass
hi! link cssVendor Keyword

call s:hi("dosiniHeader", s:tiny8_gui, "", s:tiny8_term, "", "", "")
hi! link dosiniLabel Type

call s:hi("dtBooleanKey", s:tiny7_gui, "", s:tiny7_term, "", "", "")
call s:hi("dtExecKey", s:tiny7_gui, "", s:tiny7_term, "", "", "")
call s:hi("dtLocaleKey", s:tiny7_gui, "", s:tiny7_term, "", "", "")
call s:hi("dtNumericKey", s:tiny7_gui, "", s:tiny7_term, "", "", "")
call s:hi("dtTypeKey", s:tiny7_gui, "", s:tiny7_term, "", "", "")
hi! link dtDelim Delimiter
hi! link dtLocaleValue Keyword
hi! link dtTypeValue Keyword

if g:tiny_uniform_diff_background == 0
  call s:hi("DiffAdd", s:tiny14_gui, s:tiny0_gui, s:tiny14_term, "NONE", "inverse", "")
  call s:hi("DiffChange", s:tiny13_gui, s:tiny0_gui, s:tiny13_term, "NONE", "inverse", "")
  call s:hi("DiffDelete", s:tiny11_gui, s:tiny0_gui, s:tiny11_term, "NONE", "inverse", "")
  call s:hi("DiffText", s:tiny9_gui, s:tiny0_gui, s:tiny9_term, "NONE", "inverse", "")
else
  call s:hi("DiffAdd", s:tiny14_gui, s:tiny1_gui, s:tiny14_term, s:tiny1_term, "", "")
  call s:hi("DiffChange", s:tiny13_gui, s:tiny1_gui, s:tiny13_term, s:tiny1_term, "", "")
  call s:hi("DiffDelete", s:tiny11_gui, s:tiny1_gui, s:tiny11_term, s:tiny1_term, "", "")
  call s:hi("DiffText", s:tiny9_gui, s:tiny1_gui, s:tiny9_term, s:tiny1_term, "", "")
endif
" Legacy groups for official git.vim and diff.vim syntax
hi! link diffAdded DiffAdd
hi! link diffChanged DiffChange
hi! link diffRemoved DiffDelete

call s:hi("gitconfigVariable", s:tiny7_gui, "", s:tiny7_term, "", "", "")

call s:hi("goBuiltins", s:tiny7_gui, "", s:tiny7_term, "", "", "")
hi! link goConstants Keyword

call s:hi("helpBar", s:tiny3_gui, "", s:tiny3_term, "", "", "")
call s:hi("helpHyperTextJump", s:tiny8_gui, "", s:tiny8_term, "", s:underline, "")

call s:hi("htmlArg", s:tiny7_gui, "", s:tiny7_term, "", "", "")
call s:hi("htmlLink", s:tiny4_gui, "", "", "", "NONE", "NONE")
hi! link htmlBold Bold
hi! link htmlEndTag htmlTag
hi! link htmlItalic Italic
hi! link htmlH1 markdownH1
hi! link htmlH2 markdownH1
hi! link htmlH3 markdownH1
hi! link htmlH4 markdownH1
hi! link htmlH5 markdownH1
hi! link htmlH6 markdownH1
hi! link htmlSpecialChar SpecialChar
hi! link htmlTag Keyword
hi! link htmlTagN htmlTag

call s:hi("javaDocTags", s:tiny7_gui, "", s:tiny7_term, "", "", "")
hi! link javaCommentTitle Comment
hi! link javaScriptBraces Delimiter
hi! link javaScriptIdentifier Keyword
hi! link javaScriptNumber Number

call s:hi("jsonKeyword", s:tiny7_gui, "", s:tiny7_term, "", "", "")

call s:hi("lessClass", s:tiny7_gui, "", s:tiny7_term, "", "", "")
hi! link lessAmpersand Keyword
hi! link lessCssAttribute Delimiter
hi! link lessFunction Function
hi! link cssSelectorOp Keyword

hi! link lispAtomBarSymbol SpecialChar
hi! link lispAtomList SpecialChar
hi! link lispAtomMark Keyword
hi! link lispBarSymbol SpecialChar
hi! link lispFunc Function

hi! link luaFunc Function

call s:hi("markdownBlockquote", s:tiny7_gui, "", s:tiny7_term, "", "", "")
call s:hi("markdownCode", s:tiny7_gui, "", s:tiny7_term, "", "", "")
call s:hi("markdownCodeDelimiter", s:tiny7_gui, "", s:tiny7_term, "", "", "")
call s:hi("markdownFootnote", s:tiny7_gui, "", s:tiny7_term, "", "", "")
call s:hi("markdownId", s:tiny7_gui, "", s:tiny7_term, "", "", "")
call s:hi("markdownIdDeclaration", s:tiny7_gui, "", s:tiny7_term, "", "", "")
call s:hi("markdownH1", s:tiny8_gui, "", s:tiny8_term, "", "", "")
call s:hi("markdownLinkText", s:tiny8_gui, "", s:tiny8_term, "", "", "")
call s:hi("markdownUrl", s:tiny4_gui, "", "NONE", "", "NONE", "")
hi! link markdownBold Bold
hi! link markdownBoldDelimiter Keyword
hi! link markdownFootnoteDefinition markdownFootnote
hi! link markdownH2 markdownH1
hi! link markdownH3 markdownH1
hi! link markdownH4 markdownH1
hi! link markdownH5 markdownH1
hi! link markdownH6 markdownH1
hi! link markdownIdDelimiter Keyword
hi! link markdownItalic Italic
hi! link markdownItalicDelimiter Keyword
hi! link markdownLinkDelimiter Keyword
hi! link markdownLinkTextDelimiter Keyword
hi! link markdownListMarker Keyword
hi! link markdownRule Keyword
hi! link markdownHeadingDelimiter Keyword

call s:hi("perlPackageDecl", s:tiny7_gui, "", s:tiny7_term, "", "", "")

call s:hi("phpClasses", s:tiny7_gui, "", s:tiny7_term, "", "", "")
call s:hi("phpDocTags", s:tiny7_gui, "", s:tiny7_term, "", "", "")
hi! link phpDocCustomTags phpDocTags
hi! link phpMemberSelector Keyword

call s:hi("podCmdText", s:tiny7_gui, "", s:tiny7_term, "", "", "")
call s:hi("podVerbatimLine", s:tiny4_gui, "", "NONE", "", "", "")
hi! link podFormat Keyword

hi! link pythonBuiltin Type
hi! link pythonEscape SpecialChar

call s:hi("rubyConstant", s:tiny7_gui, "", s:tiny7_term, "", "", "")
call s:hi("rubySymbol", s:tiny6_gui, "", s:tiny6_term, "", "bold", "")
hi! link rubyAttribute Identifier
hi! link rubyBlockParameterList Operator
hi! link rubyInterpolationDelimiter Keyword
hi! link rubyKeywordAsMethod Function
hi! link rubyLocalVariableOrMethod Function
hi! link rubyPseudoVariable Keyword
hi! link rubyRegexp SpecialChar

call s:hi("sassClass", s:tiny7_gui, "", s:tiny7_term, "", "", "")
call s:hi("sassId", s:tiny7_gui, "", s:tiny7_term, "", s:underline, "")
hi! link sassAmpersand Keyword
hi! link sassClassChar Delimiter
hi! link sassControl Keyword
hi! link sassControlLine Keyword
hi! link sassExtend Keyword
hi! link sassFor Keyword
hi! link sassFunctionDecl Keyword
hi! link sassFunctionName Function
hi! link sassidChar sassId
hi! link sassInclude SpecialChar
hi! link sassMixinName Function
hi! link sassMixing SpecialChar
hi! link sassReturn Keyword

hi! link shCmdParenRegion Delimiter
hi! link shCmdSubRegion Delimiter
hi! link shDerefSimple Identifier
hi! link shDerefVar Identifier

hi! link sqlKeyword Keyword
hi! link sqlSpecial Keyword

call s:hi("vimAugroup", s:tiny7_gui, "", s:tiny7_term, "", "", "")
call s:hi("vimMapRhs", s:tiny7_gui, "", s:tiny7_term, "", "", "")
call s:hi("vimNotation", s:tiny7_gui, "", s:tiny7_term, "", "", "")
hi! link vimFunc Function
hi! link vimFunction Function
hi! link vimUserFunc Function

call s:hi("xmlAttrib", s:tiny7_gui, "", s:tiny7_term, "", "", "")
call s:hi("xmlCdataStart", s:tiny3_gui, "", s:tiny3_term, "", "bold", "")
call s:hi("xmlNamespace", s:tiny7_gui, "", s:tiny7_term, "", "", "")
hi! link xmlAttribPunct Delimiter
hi! link xmlCdata Comment
hi! link xmlCdataCdata xmlCdataStart
hi! link xmlCdataEnd xmlCdataStart
hi! link xmlEndTag xmlTagName
hi! link xmlProcessingDelim Keyword
hi! link xmlTagName Keyword

call s:hi("yamlBlockMappingKey", s:tiny7_gui, "", s:tiny7_term, "", "", "")
hi! link yamlBool Keyword
hi! link yamlDocumentStart Keyword

"+----------------+
"+ Plugin Support +
"+----------------+
"+--- UI ---+
" ALE
" > w0rp/ale
call s:hi("ALEWarningSign", s:tiny13_gui, "", s:tiny13_term, "", "", "")
call s:hi("ALEErrorSign" , s:tiny11_gui, "", s:tiny11_term, "", "", "")

" GitGutter
" > airblade/vim-gitgutter
call s:hi("GitGutterAdd", s:tiny14_gui, "", s:tiny14_term, "", "", "")
call s:hi("GitGutterChange", s:tiny13_gui, "", s:tiny13_term, "", "", "")
call s:hi("GitGutterChangeDelete", s:tiny11_gui, "", s:tiny11_term, "", "", "")
call s:hi("GitGutterDelete", s:tiny11_gui, "", s:tiny11_term, "", "", "")

" Signify
" > mhinz/vim-signify
call s:hi("SignifySignAdd", s:tiny14_gui, "", s:tiny14_term, "", "", "")
call s:hi("SignifySignChange", s:tiny13_gui, "", s:tiny13_term, "", "", "")
call s:hi("SignifySignChangeDelete", s:tiny11_gui, "", s:tiny11_term, "", "", "")
call s:hi("SignifySignDelete", s:tiny11_gui, "", s:tiny11_term, "", "", "")

" fugitive.vim
" > tpope/vim-fugitive
call s:hi("gitcommitDiscardedFile", s:tiny11_gui, "", s:tiny11_term, "", "", "")
call s:hi("gitcommitUntrackedFile", s:tiny11_gui, "", s:tiny11_term, "", "", "")
call s:hi("gitcommitSelectedFile", s:tiny14_gui, "", s:tiny14_term, "", "", "")

" davidhalter/jedi-vim
call s:hi("jediFunction", s:tiny4_gui, s:tiny3_gui, "", s:tiny3_term, "", "")
call s:hi("jediFat", s:tiny8_gui, s:tiny3_gui, s:tiny8_term, s:tiny3_term, s:underline."bold", "")

" NERDTree
" > scrooloose/nerdtree
call s:hi("NERDTreeExecFile", s:tiny7_gui, "", s:tiny7_term, "", "", "")
hi! link NERDTreeDirSlash Keyword
hi! link NERDTreeHelp Comment

" CtrlP
" > ctrlpvim/ctrlp.vim
hi! link CtrlPMatch Keyword
hi! link CtrlPBufferHid Normal

" vim-plug
" > junegunn/vim-plug
call s:hi("plugDeleted", s:tiny11_gui, "", "", s:tiny11_term, "", "")

" vim-signature
" > kshenoy/vim-signature
call s:hi("SignatureMarkText", s:tiny8_gui, "", s:tiny8_term, "", "", "")

"+--- Languages ---+
" JavaScript
" > pangloss/vim-javascript
call s:hi("jsGlobalNodeObjects", s:tiny8_gui, "", s:tiny8_term, "", s:italic, "")
hi! link jsBrackets Delimiter
hi! link jsFuncCall Function
hi! link jsFuncParens Delimiter
hi! link jsThis Keyword
hi! link jsNoise Delimiter
hi! link jsPrototype Keyword
hi! link jsRegexpString SpecialChar

" Markdown
" > plasticboy/vim-markdown
call s:hi("mkdCode", s:tiny7_gui, "", s:tiny7_term, "", "", "")
call s:hi("mkdFootnote", s:tiny8_gui, "", s:tiny8_term, "", "", "")
call s:hi("mkdRule", s:tiny10_gui, "", s:tiny10_term, "", "", "")
call s:hi("mkdLineBreak", s:tiny9_gui, "", s:tiny9_term, "", "", "")
hi! link mkdBold Bold
hi! link mkdItalic Italic
hi! link mkdString Keyword
hi! link mkdCodeStart mkdCode
hi! link mkdCodeEnd mkdCode
hi! link mkdBlockquote Comment
hi! link mkdListItem Keyword
hi! link mkdListItemLine Normal
hi! link mkdFootnotes mkdFootnote
hi! link mkdLink markdownLinkText
hi! link mkdURL markdownUrl
hi! link mkdInlineURL mkdURL
hi! link mkdID Identifier
hi! link mkdLinkDef mkdLink
hi! link mkdLinkDefTarget mkdURL
hi! link mkdLinkTitle mkdInlineURL
hi! link mkdDelimiter Keyword

" Vimwiki
" > vimwiki/vimwiki
if !exists("g:vimwiki_hl_headers") || g:vimwiki_hl_headers == 0
  for s:i in range(1,6)
    call s:hi("VimwikiHeader".s:i, s:tiny8_gui, "", s:tiny8_term, "", "bold", "")
  endfor
else
  let s:vimwiki_hcolor_guifg = [s:tiny7_gui, s:tiny8_gui, s:tiny9_gui, s:tiny10_gui, s:tiny14_gui, s:tiny15_gui]
  let s:vimwiki_hcolor_ctermfg = [s:tiny7_term, s:tiny8_term, s:tiny9_term, s:tiny10_term, s:tiny14_term, s:tiny15_term]
  for s:i in range(1,6)
    call s:hi("VimwikiHeader".s:i, s:vimwiki_hcolor_guifg[s:i-1] , "", s:vimwiki_hcolor_ctermfg[s:i-1], "", "bold", "")
  endfor
endif

call s:hi("VimwikiLink", s:tiny8_gui, "", s:tiny8_term, "", s:underline, "")
hi! link VimwikiHeaderChar markdownHeadingDelimiter
hi! link VimwikiHR Keyword
hi! link VimwikiList markdownListMarker

" YAML
" > stephpy/vim-yaml
call s:hi("yamlKey", s:tiny7_gui, "", s:tiny7_term, "", "", "")

